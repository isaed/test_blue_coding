
the project include in the root folder a dump of the database "db_test_blue_coding.sql"

the database for the project can also be generated with the commands "php bin/console doctrine:database:create", "php bin/console make:migration", "php bin/console doctrine:migrations:migrate"

command to execute the server "php bin/console server:start *:9000"

the whole bussines logic is in the controller "src/Controller/HomepageController.php"

the page where the gallery is render is in "templates/homepage/index.html.twig"


