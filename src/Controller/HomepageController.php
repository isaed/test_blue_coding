<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\SearchLog;
use App\Entity\Favorites;


class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
		return $this->render('homepage/index.html.twig', [
			'images' => array(),
			'images_favorites' => array(),
			'log' => array(),
        ]);
    }

	/**
	 * @param Request $request
	 * @Route("/homepage/search", name="homepage_search")
	 * @return mixed
	 */
	public function search(Request $request)
	{

		$query= $request->get('search-box');

		$this->saveQuery($query);

		$images = $this->getImages($query);

		return $this->render('homepage/index.html.twig', [
			'images' => $images,
			'images_favorites' => array(),
			'log' => array(),
		]);
	}

	/**
	 * @param Request $request
	 * @Route("/homepage/favorites", name="homepage_favorites")
	 * @return mixed
	 */
	public function favorites(Request $request)
	{

		$urls = $request->get('url_favorites');
		$urls = ltrim($urls, ','); 
		$urls = explode(',', $urls);

        $entityManager = $this->getDoctrine()->getManager();

		foreach ($urls as $url) {
		    $favorite = new Favorites();
	        $favorite->setIdUser($this->getUserId());
	        $favorite->setUrl($url);
	        $entityManager->persist($favorite);
	        $entityManager->flush();
		}

		return $this->render('homepage/index.html.twig', [
			'images' => array(),
			'images_favorites' => array(),
			'log' => array(),
		]);
	}

	/**
	 * @Route("/homepage/show_favorites", name="homepage_show_favorites")
	 * @return mixed
	 */
	public function show_favorites()
	{

		$user_id = $this->getUserId();

		$repository = $this->getDoctrine()->getRepository(Favorites::class);

		$images = $repository->findBy([
		    'id_user' => $user_id,
		]);

		return $this->render('homepage/index.html.twig', [
			'images' => array(),
			'images_favorites' => $images,
			'log' => array(),
		]);
	}

	/**
	 * @Route("/homepage/show_query_log", name="homepage_show_query_log")
	 * @return mixed
	 */
	public function show_query_log()
	{

		$user_id = $this->getUserId();

		$repository = $this->getDoctrine()->getRepository(SearchLog::class);

		$log = $repository->findBy([
		    'id_user' => $user_id,
		]);

		return $this->render('homepage/index.html.twig', [
			'images' => array(),
			'images_favorites' => array(),
			'log' => $log,
		]);
	}

	public function getImages($query)
	{

		$giphy = new \rfreebern\Giphy();

		return $giphy->search($query, $limit = 50, $offset = 0);
	}


	public function saveQuery($query)
	{

		$search_log = new SearchLog();
        $search_log->setIdUser($this->getUserId());
        $search_log->setQuery($query);
        $search_log->setDate(new \DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($search_log);
        $entityManager->flush();

        return true;
	}

	/**
	 * Get user id
	 * @return integer $userId
	 */
	protected function getUserId()
	{
	    $user = $user = $this->get('security.token_storage')->getToken()->getUser();
	    $userId = $user->getId();

	    return $userId;
	}


}
