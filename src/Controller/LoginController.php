<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Form\LoginType;

class LoginController extends Controller
{
	/**
	 * @Route("/login", name="login")
	 */
	public function index(AuthenticationUtils $authenticationUtils)
	{
		$error = $authenticationUtils->getLastAuthenticationError();
		$lastUsername = $authenticationUtils->getLastUsername();
		$form = $this->createForm(LoginType::class);
		return $this->render('login/index.html.twig', [
			'last_username' => $lastUsername,
			'error'         => $error,
			'form'          => $form->createView(),
		]);
	}

	/**
	 * @Route("/logout", name="logout")
	 */
	public function logout() {}

	/**
	 * @Route("/login_check", name="login_check")
	 */
	public function login_check() {
	}
}
