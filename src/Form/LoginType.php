<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
			->add('_username', TextType::class, array( 
                'attr'   =>  array(
                'class'   => 'input_login')
            ))
			->add('_password', PasswordType::class, array( 
                'attr'   =>  array(
                'class'   => 'input_login')
            ))
			->add('login', SubmitType::class, ['label' => 'Login'])
		;
    }

	public function getBlockPrefix() {}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
